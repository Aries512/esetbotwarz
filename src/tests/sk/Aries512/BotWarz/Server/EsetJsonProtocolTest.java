package tests.sk.Aries512.BotWarz.Server;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import sk.Aries512.BotWarz.Game.Bot;
import sk.Aries512.BotWarz.Game.BotCommand;
import sk.Aries512.BotWarz.Game.Position;
import sk.Aries512.BotWarz.Server.EsetJsonProtocol;
import sk.Aries512.BotWarz.Server.Message;
import java.util.HashMap;
import java.util.Map;

public class EsetJsonProtocolTest {

	private EsetJsonProtocol ejp = new EsetJsonProtocol();

	@Test
	public void testDecodeConnected() throws Exception {
		String json = "{\n" +
			"   \"status\": \"socket_connected\",\n" +
			"   \"msg\":\"Hello there! You are connected to BotWaRz game server.\",\n" +
			"   \"random\":\"d039abeb6ba92bf6c8798c01d30668ff424ef1208bdcc1cc33af438b2d1c62c2\"\n" +
			"}";
		Message msg = ejp.decode(json);
		assertEquals(msg.type, Message.MessageType.LoginPrompt);
		assertEquals(msg.additionalInfo, "Hello there! You are connected to BotWaRz game server.");
	}

	@Test
	public void testDecodeLogin() throws Exception {
		String json = "{\n" +
			"\"status\": \"login_ok\"," +
			"\"msg\":\"Login was successful. Please wait, you will receive initial game data when game starts.\"" +
			"}";
		Message msg = ejp.decode(json);
		assertEquals(msg.type, Message.MessageType.LoginAnswer);
		assertEquals(msg.additionalInfo, "Login was successful. Please wait, you will receive initial game data when game starts.");
	}

	@Test
	public void testDecodeGame() throws Exception {
		String json = "{\n" +
			"    \"game\": {\n" +
			"        \"time\": 0,\n" +
			"        \"botRadius\": 20,\n" +
			"        \"world\":{\n" +
			"            \"width\":900,\n" +
			"            \"height\":600,\n" +
			"        },\n" +
			"        \"speedLevels\":[\n" +
			"            { \"speed\": 20, \"maxAngle\": 30 },\n" +
			"            { \"speed\": 50, \"maxAngle\": 15 },\n" +
			"            { \"speed\": 90, \"maxAngle\": 10 },\n" +
			"            { \"speed\": 180, \"maxAngle\": 5 },\n" +
			"            { \"speed\": 360, \"maxAngle\": 2 }\n" +
			"        ],\n" +
			"        \"players\":[\n" +
			"            {\n" +
			"                \"nickname\": \"Johnny\",\n" +
			"                \"asset\": \"fire\",\n" +
			"                \"bots\": [\n" +
			"                    { \"id\": 1, \"x\": 100, \"y\": 100, \"angle\": 0, \"speed\": 20 },\n" +
			"                    { \"id\": 2, \"x\": 100, \"y\": 200, \"angle\": 0, \"speed\": 20 },\n" +
			"                    { \"id\": 3, \"x\": 100, \"y\": 300, \"angle\": 0, \"speed\": 20 },\n" +
			"                    { \"id\": 4, \"x\": 100, \"y\": 400, \"angle\": 0, \"speed\": 20 },\n" +
			"                    { \"id\": 5, \"x\": 100, \"y\": 500, \"angle\": 0, \"speed\": 20 }\n" +
			"                ]\n" +
			"            },\n" +
			"            {\n" +
			"                \"nickname\": \"Jimmy\",\n" +
			"                \"asset\": \"spark\",\n" +
			"                \"bots\": [\n" +
			"                    { \"id\": 6, \"x\": 532, \"y\": 100, \"angle\": 180, \"speed\": 20 },\n" +
			"                    { \"id\": 7, \"x\": 532, \"y\": 200, \"angle\": 180, \"speed\": 20 },\n" +
			"                    { \"id\": 8, \"x\": 532, \"y\": 300, \"angle\": 180, \"speed\": 20 },\n" +
			"                    { \"id\": 9, \"x\": 532, \"y\": 400, \"angle\": 180, \"speed\": 20 },\n" +
			"                    { \"id\": 10, \"x\": 532, \"y\": 500, \"angle\": 180, \"speed\": 20 }\n" +
			"                ]\n" +
			"            }\n" +
			"        ]\n" +
			"    }, \n" +
			"}";
		ejp.createLoginMessage("Johnny", "");
		Message msg = ejp.decode(json);
		assertEquals(msg.type, Message.MessageType.GameStart);
		assertEquals(msg.time, 0);
		assertEquals(msg.gameConstants.botRadius, 20);
		assertEquals(msg.gameConstants.worldWidth, 900);
		assertEquals(msg.gameConstants.worldHeight, 600);
		assertEquals(msg.gameConstants.speedLevels[1].speed, 20);
		assertEquals(msg.gameConstants.speedLevels[1].maxAngle, 30);
		assertEquals(msg.gameConstants.speedLevels[5].speed, 360);
		assertEquals(msg.gameConstants.speedLevels[5].maxAngle, 2);
		assertEquals(msg.playerBots.get(0).id, 1);
		assertEquals(msg.playerBots.get(0).position.x, 100, 0.01);
		assertEquals(msg.playerBots.get(0).position.y, 100, 0.01);
		assertEquals(msg.playerBots.get(0).orientation, 0);
		assertEquals(msg.playerBots.get(0).speed, 20);
		assertEquals(msg.playerBots.get(4).id, 5);
		assertEquals(msg.enemyBots.get(0).id, 6);
		assertEquals(msg.enemyBots.get(0).position.x, 532, 0.01);
		assertEquals(msg.enemyBots.get(0).position.y, 100, 0.01);
		assertEquals(msg.enemyBots.get(0).orientation, 180);
		assertEquals(msg.enemyBots.get(0).speed,20 );
		assertEquals(msg.enemyBots.get(4).id, 10);

		json = "{\n" +
			"    \"play\":{\n" +
			"        \"time\":1931,\n" +
			"        \"players\":[\n" +
			"            {\n" +
			"                \"nickname\": \"Johnny\",\n" +
			"                \"bots\":[\n" +
			"                    { \"id\":1, \"x\":310.28, \"y\":48.24, \"angle\":-45, \"speed\": 20},\n" +
			"                    { \"id\":2, \"x\":289.54, \"y\":301.82, \"angle\":45, \"speed\": 20},\n" +
			"                    { \"id\":3, \"x\":331.72, \"y\":300, \"angle\":0, \"speed\": 50},\n" +
			"                    { \"id\":4, \"x\":331.72, \"y\":400, \"angle\":0, \"speed\": 180},\n" +
			"                    { \"id\":5, \"x\":276.86, \"y\":529, \"angle\":45, \"speed\": 360}\n" +
			"                ]\n" +
			"            },\n" +
			"            {\n" +
			"                \"nickname\": \"Jimmy\",\n" +
			"                \"bots\":[\n" +
			"                    { \"id\":1, \"x\":700.28, \"y\":100, \"angle\":180, \"speed\": 50},\n" +
			"                    { \"id\":2, \"x\":717.15, \"y\":159.27, \"angle\":180, \"speed\": 180},\n" +
			"                    { \"id\":5, \"x\":762.88, \"y\":484.88, \"angle\":-135, \"speed\": 20}\n" +
			"                ]\n" +
			"            },\n" +
			"        ],\n" +
			"       \"lastCmdId\":1234\n" +
			"   }\n" +
			"}";

		msg = ejp.decode(json);
		assertEquals(msg.time, 1931);
		assertEquals(msg.playerBots.get(0).id, 1);
		assertEquals(msg.playerBots.get(0).position.x, 310.28, 0.01);
		assertEquals(msg.playerBots.get(0).position.y, 48.24, 0.01);
		assertEquals(msg.playerBots.get(0).orientation, -45);
		assertEquals(msg.playerBots.get(0).speed, 20);
		assertEquals(msg.playerBots.get(4).id, 5);
		assertEquals(msg.enemyBots.get(0).id, 1);
		assertEquals(msg.enemyBots.get(0).position.x, 700.28, 0.01);
		assertEquals(msg.enemyBots.get(0).position.y, 100, 0.01);
		assertEquals(msg.enemyBots.get(0).orientation, 180);
		assertEquals(msg.enemyBots.get(0).speed, 50);
		assertEquals(msg.enemyBots.get(2).id, 5);

		json = "{\n" +
			"   \"result\": {\n" +
			"        \"time\": 9986,\n" +
			"        \"status\": \"Johnny won!\",\n" +
			"        \"winner\": {\n" +
			"            \"nickname\": \"Johnny\",\n" +
			"            \"bots\": [\n" +
			"                { \"id\": 1, \"x\": 697.28, \"y\": 72, \"angle\": -180},\n" +
			"                { \"id\": 2, \"x\": 172.55, \"y\": 243.43, \"angle\": -135},\n" +
			"                { \"id\": 5, \"x\": 170.65, \"y\": 277.54, \"angle\": -180 }\n" +
			"            ]\n" +
			"        }\n" +
			"    } \n" +
			"}";

		msg = ejp.decode(json);
		assertEquals(msg.time, 9986);
		assertEquals(msg.winnerNickname, "Johnny");
		assertEquals(msg.playerBots.get(0).id, 1);
		assertEquals(msg.playerBots.get(0).position.x, 697.28, 0.001);
		assertEquals(msg.playerBots.get(0).position.y, 72, 0.001);
		assertEquals(msg.playerBots.get(0).orientation, -180);
		assertEquals(msg.playerBots.get(2).id, 5);
	}

	@Test
	public void testDecodeMessages() throws Exception {
		String json = "{\n" +
			"   status: \"command_error_json\",\n" +
			"   msg:\"I was unable to parse your command (not a valid JSON). Your connection has been terminated!\"\n" +
			"}";

		Message msg = ejp.decode(json);
		assertEquals(msg.type, Message.MessageType.TerminationError);
		assertEquals(msg.additionalInfo, "I was unable to parse your command (not a valid JSON). Your connection has been terminated!");

		json = "{\n" +
			"   status: \"command_time_error\",\n" +
			"   msg:\"Delay between commands have to be at least 200ms (your delay was 180ms). You've been penalized...\"\n" +
			"}";

		msg = ejp.decode(json);
		assertEquals(msg.type, Message.MessageType.TimeError);
		assertEquals(msg.additionalInfo, "Delay between commands have to be at least 200ms (your delay was 180ms). You've been penalized...");
	}

	@Test
	public void testCreateLoginMessage() throws Exception {
		assertEquals(ejp.createLoginMessage("abc", "def"), "{\"login\":{\"nickname\":\"abc\",\"hash\":\"def\"}}\n");
	}

	@Test
	public void testCreateMessageBotUpdate() throws Exception {
		Map<Bot, BotCommand> cmds = new HashMap<Bot, BotCommand>();
		cmds.put(new Bot(1, new Position(2,3), 20, 1), new BotCommand(BotCommand.BotCommandType.Accelerate));
		cmds.put(new Bot(2, new Position(4,8), 20, -100), new BotCommand(BotCommand.BotCommandType.Turn, 10));
		assertEquals(ejp.createMessageBotUpdate(cmds, "123"),
			"{\"bots\":[{\"id\":1,\"cmd\":\"accelerate\"},{\"angle\":10,\"id\":2,\"cmd\":\"steer\"}],\"cmdId\":\"123\"}\n");

	}
}
