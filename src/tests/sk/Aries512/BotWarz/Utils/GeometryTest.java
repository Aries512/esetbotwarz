package tests.sk.Aries512.BotWarz.Utils;
import static org.junit.Assert.*;
import sk.Aries512.BotWarz.Game.Position;
import sk.Aries512.BotWarz.Utils.Geometry;

public class GeometryTest {

	// TODO: expected and actual are reversed, too lazy to fix it

	@org.junit.Test
	public void testDistSq() throws Exception {
		assertEquals(Geometry.distSq(0, 0, 0, 0), 0);
		assertEquals(Geometry.distSq(100, -13, 100, -13), 0);
		assertEquals(Geometry.distSq(1, 0, 0, 0), 1);
		assertEquals(Geometry.distSq(0, 1, 0, 0), 1);
		assertEquals(Geometry.distSq(0, 0, 1, 0), 1);
		assertEquals(Geometry.distSq(0, 0, 0, 1), 1);
		assertEquals(Geometry.distSq(3, 0, 0, 4), 5 * 5);
		assertEquals(Geometry.distSq(33.7f, 5.6f, 87.5f, 113.66f), 14571);
		assertEquals(Geometry.distSq(-33.7f, -5.6f, -87.5f, -113.66f), 14571);
	}

	@org.junit.Test
	public void testIsOrientedUp() throws Exception {
		assertFalse(Geometry.isOrientedUp(0));
		assertFalse(Geometry.isOrientedUp(1));
		assertFalse(Geometry.isOrientedUp(45));
		assertFalse(Geometry.isOrientedUp(90));
		assertFalse(Geometry.isOrientedUp(135));
		assertFalse(Geometry.isOrientedUp(179));
		assertFalse(Geometry.isOrientedUp(180));
		assertTrue(Geometry.isOrientedUp(-1));
		assertTrue(Geometry.isOrientedUp(-45));
		assertTrue(Geometry.isOrientedUp(-90));
		assertTrue(Geometry.isOrientedUp(-135));
		assertTrue(Geometry.isOrientedUp(-179));
		assertFalse(Geometry.isOrientedUp(-180));
	}

	@org.junit.Test
	public void testIsOrientedDown() throws Exception {
		assertFalse(Geometry.isOrientedDown(0));
		assertTrue(Geometry.isOrientedDown(1));
		assertTrue(Geometry.isOrientedDown(45));
		assertTrue(Geometry.isOrientedDown(90));
		assertTrue(Geometry.isOrientedDown(135));
		assertTrue(Geometry.isOrientedDown(179));
		assertFalse(Geometry.isOrientedDown(180));
		assertFalse(Geometry.isOrientedDown(-1));
		assertFalse(Geometry.isOrientedDown(-45));
		assertFalse(Geometry.isOrientedDown(-90));
		assertFalse(Geometry.isOrientedDown(-135));
		assertFalse(Geometry.isOrientedDown(-179));
		assertFalse(Geometry.isOrientedDown(-180));
	}

	@org.junit.Test
	public void testIsOrientedLeft() throws Exception {
		assertFalse(Geometry.isOrientedLeft(0));
		assertFalse(Geometry.isOrientedLeft(1));
		assertFalse(Geometry.isOrientedLeft(45));
		assertFalse(Geometry.isOrientedLeft(90));
		assertTrue(Geometry.isOrientedLeft(135));
		assertTrue(Geometry.isOrientedLeft(179));
		assertTrue(Geometry.isOrientedLeft(180));
		assertFalse(Geometry.isOrientedLeft(-1));
		assertFalse(Geometry.isOrientedLeft(-45));
		assertFalse(Geometry.isOrientedLeft(-90));
		assertTrue(Geometry.isOrientedLeft(-135));
		assertTrue(Geometry.isOrientedLeft(-179));
		assertTrue(Geometry.isOrientedLeft(-180));
	}

	@org.junit.Test
	public void testIsOrientedRight() throws Exception {
		assertTrue(Geometry.isOrientedRight(0));
		assertTrue(Geometry.isOrientedRight(1));
		assertTrue(Geometry.isOrientedRight(45));
		assertFalse(Geometry.isOrientedRight(90));
		assertFalse(Geometry.isOrientedRight(135));
		assertFalse(Geometry.isOrientedRight(179));
		assertFalse(Geometry.isOrientedRight(180));
		assertTrue(Geometry.isOrientedRight(-1));
		assertTrue(Geometry.isOrientedRight(-45));
		assertFalse(Geometry.isOrientedRight(-90));
		assertFalse(Geometry.isOrientedRight(-135));
		assertFalse(Geometry.isOrientedRight(-179));
		assertFalse(Geometry.isOrientedRight(-180));
	}

	@org.junit.Test
	public void testNormalizeOrientation() throws Exception {
		assertEquals(Geometry.normalizeOrientation(0), 0);
		assertEquals(Geometry.normalizeOrientation(1), 1);
		assertEquals(Geometry.normalizeOrientation(-1), -1);
		assertEquals(Geometry.normalizeOrientation(90), 90);
		assertEquals(Geometry.normalizeOrientation(-90), -90);
		assertEquals(Geometry.normalizeOrientation(179), 179);
		assertEquals(Geometry.normalizeOrientation(-179), -179);
		assertEquals(Geometry.normalizeOrientation(180), 180);
		assertEquals(Geometry.normalizeOrientation(-180), -180);
		assertEquals(Geometry.normalizeOrientation(181), -179);
		assertEquals(Geometry.normalizeOrientation(270), -90);
		assertEquals(Geometry.normalizeOrientation(360), 0);
		assertEquals(Geometry.normalizeOrientation(710), -10);
		assertEquals(Geometry.normalizeOrientation(720), 0);
		assertEquals(Geometry.normalizeOrientation(730), 10);
		assertEquals(Geometry.normalizeOrientation(-181), 179);
		assertEquals(Geometry.normalizeOrientation(-270), 90);
		assertEquals(Geometry.normalizeOrientation(-360), 0);
		assertEquals(Geometry.normalizeOrientation(-710), 10);
		assertEquals(Geometry.normalizeOrientation(-720), 0);
		assertEquals(Geometry.normalizeOrientation(-730), -10);
	}

	private boolean comparePosition(Position p1, Position p2) {
		return Math.abs(p1.x - p2.x) < 1e-4 && Math.abs(p1.y - p2.y) < 1e-4;
	}

	@org.junit.Test
	public void testProjectPoint() throws Exception {
		assertTrue(comparePosition(Geometry.projectPoint(0, 0, 0, 10), new Position(10, 0)));
		assertTrue(comparePosition(Geometry.projectPoint(0, 0, 45, 10), new Position(0.70710678118f * 10, 0.70710678118f * 10)));
		assertTrue(comparePosition(Geometry.projectPoint(0, 0, 90, 10), new Position(0, 10)));
		assertTrue(comparePosition(Geometry.projectPoint(0, 0, 180, 10), new Position(-10, 0)));
		assertTrue(comparePosition(Geometry.projectPoint(0, 0, -90, 10), new Position(0, -10)));
		assertTrue(comparePosition(Geometry.projectPoint(117, -4, 57, 23), new Position(117 + 12.5266978f, -4 + 19.289423062f)));
	}

	@org.junit.Test
	public void testDistFromVerticalLine() throws Exception {
		assertEquals(Geometry.distFromVerticalLine(0, 0, 100), 100);
		assertEquals(Geometry.distFromVerticalLine(14, 0, 100), 86);
		assertEquals(Geometry.distFromVerticalLine(0, 0, 1), 1);
		assertEquals(Geometry.distFromVerticalLine(0, 0, -100), Integer.MAX_VALUE);
		assertEquals(Geometry.distFromVerticalLine(0, 90, 100), Integer.MAX_VALUE);
		assertEquals(Geometry.distFromVerticalLine(0, -90, 100), Integer.MAX_VALUE);
		assertEquals(Geometry.distFromVerticalLine(0, 0, 0), 0);
		assertEquals(Geometry.distFromVerticalLine(0, 90, 0), 0);
		assertEquals(Geometry.distFromVerticalLine(0, -90, 0), 0);
		assertEquals(Geometry.distFromVerticalLine(0, 45, 100), (int)(100 / Math.cos(Math.toRadians(45))));
		assertEquals(Geometry.distFromVerticalLine(0, -45, 100), (int)(100 / Math.cos(Math.toRadians(45))));
		assertEquals(Geometry.distFromVerticalLine(0, 59, 100), (int)(100 / Math.cos(Math.toRadians(59))));
		assertEquals(Geometry.distFromVerticalLine(0, 135, 100), Integer.MAX_VALUE);
	}

	@org.junit.Test
	public void testDistFromHorizontalLine() throws Exception {
		assertEquals(Geometry.distFromHorizontalLine(0, 90, 100), 100);
		assertEquals(Geometry.distFromHorizontalLine(14, 90, 100), 86);
		assertEquals(Geometry.distFromHorizontalLine(0, 90, 1), 1);
		assertEquals(Geometry.distFromHorizontalLine(0, 90, -100), Integer.MAX_VALUE);
		assertEquals(Geometry.distFromHorizontalLine(0, 0, 100), Integer.MAX_VALUE);
		assertEquals(Geometry.distFromHorizontalLine(0, 180, 100), Integer.MAX_VALUE);
		assertEquals(Geometry.distFromHorizontalLine(0, -180, 100), Integer.MAX_VALUE);
		assertEquals(Geometry.distFromHorizontalLine(0, 0, 0), 0);
		assertEquals(Geometry.distFromHorizontalLine(0, 90, 0), 0);
		assertEquals(Geometry.distFromHorizontalLine(0, -90, 0), 0);
		assertEquals(Geometry.distFromHorizontalLine(0, 45, 100), (int)(100 / Math.sin(Math.toRadians(45))));
		assertEquals(Geometry.distFromHorizontalLine(0, -45, -100), (int)(100 / Math.sin(Math.toRadians(45))));
		assertEquals(Geometry.distFromHorizontalLine(0, 59, 100), (int)(100 / Math.sin(Math.toRadians(59))));
		assertEquals(Geometry.distFromHorizontalLine(0, -135, 100), Integer.MAX_VALUE);
	}

	@org.junit.Test
	public void testVectorOrientation() throws Exception {
		assertEquals(Geometry.vectorOrientation(0, 0, 10, 0), 0);
		assertEquals(Geometry.vectorOrientation(0, 0, 10, 10), 45);
		assertEquals(Geometry.vectorOrientation(0, 0, 10, -10), -45);
		assertEquals(Geometry.vectorOrientation(0, 0, 0, 10), 90);
		assertEquals(Geometry.vectorOrientation(0, 0, 0, -10), -90);
		assertEquals(Geometry.vectorOrientation(0, 0, -10, 10), 135);
		assertEquals(Geometry.vectorOrientation(0, 0, -10, -10), -135);
		assertEquals(Geometry.vectorOrientation(0, 0, -10, 0), 180);
		assertEquals(Geometry.vectorOrientation(10, 10, 10 - 1.7632698f, 0), -100);
		assertEquals(Geometry.vectorOrientation(5, 5, 15, 5 - 20.5030384f), -64);
		assertEquals(Geometry.vectorOrientation(-3, -3, 60, -3 + 14.5446960f), 12);
		assertEquals(Geometry.vectorOrientation(100, 100, 95, 100 + 5.95876796f), 130);

	}

	@org.junit.Test
	public void testAngleBetween() throws Exception {
		assertEquals(Geometry.angleBetween(0, 0, 0, 0, 100), 90);
		assertEquals(Geometry.angleBetween(0, 0, 0, 0, -100), 90);
		assertEquals(Geometry.angleBetween(50, 0, 0, 50, 100), 90);
		assertEquals(Geometry.angleBetween(0, 0, 0, 100, 100), 45);
		assertEquals(Geometry.angleBetween(0, 0, 0, -100, 100), 135);
		assertEquals(Geometry.angleBetween(0, 0, 45, -100, 100), 90);
	}

	@org.junit.Test
	public void testDistanceUntilBorder() throws Exception {
		assertEquals(Geometry.distanceUntilBorder(100, 100, 0, 20, 0, 200, 0, 200), 80);
		assertEquals(Geometry.distanceUntilBorder(100, 100, 180, 20, 0, 200, 0, 200), 80);
		assertEquals(Geometry.distanceUntilBorder(100, 100, 90, 20, 0, 200, 0, 200), 80);
		assertEquals(Geometry.distanceUntilBorder(100, 100, -90, 20, 0, 200, 0, 200), 80);
		assertEquals(Geometry.distanceUntilBorder(100, 20, -90, 20, 0, 200, 0, 200), 0);
		assertEquals(Geometry.distanceUntilBorder(100, 10, -90, 20, 0, 200, 0, 200), 0);
		assertEquals(Geometry.distanceUntilBorder(170, 100, -45, 20, 0, 200, 0, 200), Math.round(10 / Math.sin(Math.toRadians(45))), 0.01);
		assertEquals(Geometry.distanceUntilBorder(100, 30, -45, 20, 0, 200, 0, 200), Math.round(10 / Math.sin(Math.toRadians(45))), 0.01);
		assertEquals(Geometry.distanceUntilBorder(170, 30, -45, 20, 0, 200, 0, 200), Math.round(10 / Math.sin(Math.toRadians(45))), 0.01);
		assertEquals(Geometry.distanceUntilBorder(170, 170, -45, 20, 0, 200, 0, 200), Math.round(10 / Math.sin(Math.toRadians(45))), 0.01);
		assertEquals(Geometry.distanceUntilBorder(170, 30, 140, 20, 0, 200, 0, 200), Math.round(149 / Math.sin(Math.toRadians(50))), 0.01);
		assertEquals(Geometry.distanceUntilBorder(50, 30, 140, 20, 0, 200, 0, 200), Math.round(30 / Math.cos(Math.toRadians(40))), 0.01);

	}
}
