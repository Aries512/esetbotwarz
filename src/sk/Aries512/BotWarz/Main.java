package sk.Aries512.BotWarz;

import sk.Aries512.BotWarz.Game.Ai.Ai;
import sk.Aries512.BotWarz.Game.Ai.TurnEvalAi;
import sk.Aries512.BotWarz.Game.GameConstants;
import sk.Aries512.BotWarz.Game.GameRunner;
import sk.Aries512.BotWarz.Server.EsetJsonProtocol;
import sk.Aries512.BotWarz.Server.EsetServerConnector;
import sk.Aries512.BotWarz.Server.GameServerException;
import sk.Aries512.BotWarz.Server.Server;
import sk.Aries512.BotWarz.Ui.GameUi;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Locale;

public class Main {

	public static void main(String[] args) throws GameServerException, IOException {

		Locale.setDefault(Locale.US);

		GameConstants gameConstants = new GameConstants();
		//Server server = new LocalServer(gameConstants, new RandomAi());
		String token = Files.readAllLines(Paths.get("token")).get(0);
		Server server = new EsetServerConnector("botwarz.eset.com", 8080, "Aries512", token, new EsetJsonProtocol());
		Ai ai = new TurnEvalAi(gameConstants);
		//Ai ai = new RandomAi();
		GameUi ui = new GameUi(gameConstants);
		ui.show();
		new GameRunner(server, ai, ui).start();
	}
}
