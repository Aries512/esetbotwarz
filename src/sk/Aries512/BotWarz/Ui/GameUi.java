package sk.Aries512.BotWarz.Ui;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sk.Aries512.BotWarz.Game.Bot;
import sk.Aries512.BotWarz.Game.GameConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class GameUi {

	private Stage stage;
	private Pane pane;
	// maps bot ids to their corresponding ui elements
	private HashMap<Integer, Group> uiObjects;
	private GameConstants gameConstants;


	public GameUi(GameConstants gameConstants) {
		this.gameConstants = gameConstants;
		uiObjects = new HashMap<Integer, Group>();
		FXUtils.runFX(() -> {
			stage = new Stage();
			stage.setTitle("BotWarz UI");
			pane = new Pane();
		});
	}


	public void setGameConstants(GameConstants gameConstants) {
		this.gameConstants = gameConstants;
	}


	public void show() {
		FXUtils.runFX(() -> {
			stage.setScene(new Scene(pane, gameConstants.worldWidth, gameConstants.worldHeight));
			stage.show();
		});
	}

	public void close() {
		FXUtils.runFX(() -> stage.close());
	}


	public void display(List<Bot> playerBots, List<Bot> enemyBots) {
		FXUtils.runFX(() -> {
			List<Bot> all = new ArrayList<Bot>(playerBots);
			all.addAll(enemyBots);
			for (Bot bot: all) {
				boolean enemy = enemyBots.contains(bot);
				// new bot, create and add ui element
				if (!uiObjects.containsKey(bot.id)) {
					Group group = createBotUiElement(bot.id,enemy ? Color.RED : Color.BLUE, bot.orientation);
					uiObjects.put(bot.id, group);
					pane.getChildren().add(group);
				}
				updateUiElement(uiObjects.get(bot.id), bot);
			}
			// remove ui elements that are not present in this update
			Set<Integer> ids = all.stream().map(bot -> bot.id).collect(Collectors.toSet());
			// must copy to new list to avoid concurrent modification
			new ArrayList<Integer>(uiObjects.keySet()).stream().filter(id -> !ids.contains(id)).forEach(id -> {
				pane.getChildren().remove(uiObjects.get(id));
				uiObjects.remove(id);
			});
		});
	}


	private Group createBotUiElement(int id, Color color, int orientation) {
		Group group = new Group();
		int radius = gameConstants.botRadius;
		Circle circle = new Circle(0, 0, radius, color);
		circle.setStroke(Color.BLACK);
		circle.setStrokeWidth(1);
		Arc arc = new Arc(0, 0, radius, radius, convertOrientation(orientation) + 45, 90);
		arc.setType(ArcType.ROUND);
		Text text = new Text(Integer.toString(id));
		text.setX(radius);
		text.setY(radius);
		group.getChildren().addAll(circle, arc, text);
		return group;
	}

	private void updateUiElement(Group group, Bot bot) {
		group.setLayoutX(bot.position.x);
		group.setLayoutY(bot.position.y);
		Arc arc = (Arc)group.getChildren().stream().filter(e -> e instanceof Arc).findFirst().get();
		arc.setStartAngle(convertOrientation(bot.orientation) + 45);
		Text text = (Text)group.getChildren().stream().filter(e -> e instanceof Text).findFirst().get();
		text.setText(bot.id + ", " +  "\n" + bot.orientation + ", " + bot.speed);
	}

	// for FX representation (starting upwards, going counterclockwise)
	private int convertOrientation(int orientation) {
		orientation = (orientation + 360) % 360;
		return 360 - orientation - 90;
	}
}
