package sk.Aries512.BotWarz.Ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import java.util.concurrent.Semaphore;

/**
 * Used to run JavaFX GUI without having to create JavaFX application and from any thread.
 * http://a-hackers-craic.blogspot.sk/2014/08/starting-javafx-from-random-java-code.html
 */
public class FXUtils {

	private static FXApplication app;
	private static Semaphore semaphore = new Semaphore(0);

	public static void runFX(Runnable runnable) {
		if (app == null) {
			try {
				new Thread(() -> FXApplication.start(runnable)).start();
				semaphore.acquire();
			} catch (InterruptedException ex) {
			}
		} else {
			Platform.runLater(runnable);
		}
	}


	// must be public, otherwise FX will crash
	public static class FXApplication extends Application {

		private static Runnable runnable;

		@Override
		public void start(Stage stage) throws Exception {
			app = this;
			runnable.run();
			semaphore.release();
		}

		public static void start(Runnable runnable) {
			FXApplication.runnable = runnable;
			// Application.launch() can only be called from a static
			// method from a class that extends Application
			Application.launch();
		}
	}
}
