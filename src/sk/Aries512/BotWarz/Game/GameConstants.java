package sk.Aries512.BotWarz.Game;

public class GameConstants {

	public int worldWidth = 900;
	public int worldHeight = 600;

	public int botRadius = 20;

	public static class SpeedLevel {
		public int speed;
		public int maxAngle;
		public SpeedLevel(int speed, int maxAngle) {
			this.speed = speed;
			this.maxAngle = maxAngle;
		}
	}

	public SpeedLevel[] speedLevels = new SpeedLevel[] {
		new SpeedLevel(0, 0),
		new SpeedLevel(20, 30),
		new SpeedLevel(50, 15),
		new SpeedLevel(90, 10),
		new SpeedLevel(180, 5),
		new SpeedLevel(360, 2),
	};

	public int getSpeedLevelBySpeed(int speed) {
		for (int i = 0; i < speedLevels.length; i++) {
			if (speedLevels[i].speed == speed) {
				return i;
			}
		}
		return 0;
	}
}
