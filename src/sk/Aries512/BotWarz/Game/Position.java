package sk.Aries512.BotWarz.Game;

import sk.Aries512.BotWarz.Utils.Geometry;

public class Position {

	public float x, y;

	public Position(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public int distanceSqTo(Position p) {
		return Geometry.distSq(p.x, p.y, x, y);
	}

	@Override
	public String toString() {
		return "[" + x + "," + y + "]";
	}
}
