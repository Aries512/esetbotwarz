package sk.Aries512.BotWarz.Game;

public class BotCommand {

	public enum BotCommandType {
		Accelerate,
		Decelerate,
		Turn,
		Noop,
	}

	public BotCommandType type;
	public int amount;

	public BotCommand(BotCommandType type) {
		this.type = type;
		this.amount = 0;
	}

	public BotCommand(BotCommandType type, int amount) {
		this.type = type;
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "{" + type + ", " + amount + '}';
	}
}
