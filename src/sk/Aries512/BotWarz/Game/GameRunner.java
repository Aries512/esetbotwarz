package sk.Aries512.BotWarz.Game;

import sk.Aries512.BotWarz.Game.Ai.Ai;
import sk.Aries512.BotWarz.Server.ErrorType;
import sk.Aries512.BotWarz.Server.GameServerException;
import sk.Aries512.BotWarz.Server.Server;
import sk.Aries512.BotWarz.Ui.GameUi;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

/**
 * Controls the game flow, i.e. when to dispatch commands to the server, when to render scene in cas we use UI, etc.
 */
public class GameRunner implements GameProgressListener {

	private Server server;
	private Ai ai;
	private GameUi ui;

	public GameRunner(Server server, Ai ai) {
		this.server = server;
		server.subscribeGameProgressListener(this);
		this.ai = ai;
	}

	public GameRunner(Server server, Ai ai, GameUi ui) {
		this(server, ai);
		this.ui = ui;
	}



	public void start() throws GameServerException {
		synchronized (this) {
			server.initialize();
		}
	}



	@Override
	public void onGameStart(long time, GameConstants gameConstants, List<Bot> playerBots, List<Bot> enemyBots) {
		synchronized (this) {
			ai.setGameConstants(gameConstants);
			if (ui != null) {
				ui.setGameConstants(gameConstants);
				ui.display(playerBots, enemyBots);
			}
			dispatchCommands(playerBots, enemyBots, System.currentTimeMillis());
		}
	}



	private Semaphore commandCalculationLock = new Semaphore(1);
	private volatile long lastCommandDispatchTime = 0;

	@Override
	public void onGameProgress(long time, String lastCmdId, List<Bot> playerBots, List<Bot> enemyBots) {
		synchronized (this) {

			if (ui != null) {
				ui.display(playerBots, enemyBots);
			}

			long fromLastDispatch = System.currentTimeMillis() - lastCommandDispatchTime;
			int expectedCalculationTime = ai.getExpectedTurnCalculationDuration(playerBots, enemyBots);
			long nextCommandTime = lastCommandDispatchTime + server.getDelayBetweenCommands();
			// if we will finish after the commands can be dispatched or
			// if we are close enough to the next possible dispatch
			boolean worthCalculating = fromLastDispatch + expectedCalculationTime >= nextCommandTime
				|| fromLastDispatch >= server.getDelayBetweenCommands() / 2;

			if (worthCalculating) {
				dispatchCommands(playerBots, enemyBots, nextCommandTime);
			}
		}
	}



	private boolean dispatchCommands(List<Bot> playerBots, List<Bot> enemyBots, long nextCommandTime) {
		if (commandCalculationLock.tryAcquire()) {
			new Thread(() -> {
				int millisUntilDispatch = (int)Math.max(0, nextCommandTime - System.currentTimeMillis());
				// simulate for the time depending on when we will be able to dispatch another set of commands.
				// however, we don't want to simulate for too long, because our simulation would be outdated,
				// as enemy can meanwhile move his bots too.
				int aiSimulationDuration = 3 * (server.getDelayBetweenCommands() + millisUntilDispatch) / 4;
				// simulate at least for 100ms, but not too much in case we have huge delays for some reason
				aiSimulationDuration = Math.max(100, Math.min(aiSimulationDuration, 3 * server.getDelayBetweenCommands() / 2));
				Map<Bot, BotCommand> commands = ai.calculateNextTurn(playerBots, enemyBots, aiSimulationDuration, millisUntilDispatch);
				// if we still need to wait for the dispatch
				long needWait = nextCommandTime - System.currentTimeMillis();
				if (needWait > 0) {
					try {
						Thread.sleep(needWait);
					} catch (InterruptedException e) {
					}
				}
				try {
					server.dispatchCommands(commands);
				} catch (GameServerException e) {
					System.err.println("Error when dispatching: " + e.getMessage());
				}
				lastCommandDispatchTime = System.currentTimeMillis();
				commandCalculationLock.release();
			}).start();
			return true;
		} else {
			return false;
		}
	}



	@Override
	public void onGameEnd(long time, GameResult result, List<Bot> remainingBots) {
		System.out.println("Result: " + result);
		server.unsubscribeGameProgressListener(this);
	}

	@Override
	public void onGameTermination(long time, String message) {
		System.err.println("Terminated: " + (message == null ? "" : message));
		server.unsubscribeGameProgressListener(this);
	}

	@Override
	public void onCommandError(long time, ErrorType error, String message) {
		System.err.println("Command error!: " + (message == null ? "" : message));
	}
}
