package sk.Aries512.BotWarz.Game;

public class GameEvent {

	public enum GameEventType {
		Destruction,
		Bounce
	}

	public GameEventType type;
	public Bot source;
	public Bot target;

	public GameEvent(GameEventType type, Bot source, Bot target) {
		this.type = type;
		this.source = source;
		this.target = target;
	}

}
