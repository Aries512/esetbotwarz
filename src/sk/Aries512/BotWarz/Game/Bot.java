package sk.Aries512.BotWarz.Game;

import sk.Aries512.BotWarz.Utils.Geometry;

public class Bot {

	// fields are public because bot is used in ai computation which is performance-demanding.
	// accessing fields directly instead of using getters made everuthing run 50% faster...
	// so method call overhead was pretty big.
	// however, setter should be still preferred to direct field value change.

	public int id;
	public Position position;
	public int orientation;
	public int speed;

	public Bot(int id, Position position, int speed, int orientation) {
		this.id = id;
		setPosition(position);
		setOrientation(orientation);
		setSpeed(speed);
	}

	public Bot(Bot bot) {
		this.id = bot.id;
		setPosition(new Position(bot.position.x, bot.position.y));
		setOrientation(bot.orientation);
		setSpeed(bot.speed);
	}



	public void applyCommand(BotCommand command, GameConstants gameConstants) {
		int currSpeedLevel = gameConstants.getSpeedLevelBySpeed(speed);
		int maxSpeedLevel = gameConstants.speedLevels.length - 1;
		switch (command.type) {
			case Accelerate:
				if (currSpeedLevel < maxSpeedLevel) {
					this.speed = gameConstants.speedLevels[currSpeedLevel + 1].speed;
				}
				break;
			case Decelerate:
				if (currSpeedLevel > 1) {
					this.speed = gameConstants.speedLevels[currSpeedLevel - 1].speed;
				}
				break;
			case Turn:
				setOrientation(orientation + command.amount);
				break;
		}
	}



	public void setPosition(Position position) {
		this.position = position;
	}

	public void setOrientation(int orientation) {
		this.orientation = Geometry.normalizeOrientation(orientation);
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Bot bot = (Bot) o;
		if (id != bot.id) return false;
		return true;
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public String toString() {
		return "{" +
			"id=" + id +
			", position=" + position +
			", orientation=" + orientation +
			", speed=" + speed +
			'}';
	}
}
