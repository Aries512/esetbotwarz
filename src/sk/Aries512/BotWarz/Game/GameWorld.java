package sk.Aries512.BotWarz.Game;

import sk.Aries512.BotWarz.Utils.Geometry;
import java.util.*;

/**
 * Implements the game itself locally for simulation purposes.
 * Handles movement, collisions, etc.
 */
public class GameWorld {

	// everything is implemented to be fast, so some design decisions are less beautiful and elegant I'd wish for.
	// this concerns mainly the way how bots are stored and handled (arrays instead of lists, no lambdas and streams).

	private GameConstants gameConstants;
	/**
	 * All bots in the game (both player and enemy).
	 */
	private Bot[] bots;
	/**
	 * Whether the bot belongs to the player or the enemy.
	 */
	private boolean[] isPlayer;
	/**
	 * Whether the bot is dead. Used because it is faster than using lists and removing bots.
	 * This class has usually short lifetime (only one AI step), so not many bots will probably die.
	 */
	private boolean[] isBotDead;

	/**
	 * Default bot configuration.
	 */
	public GameWorld(GameConstants gameConstants) {
		this.gameConstants = gameConstants;
		int speed = gameConstants.speedLevels[1].speed;
		bots = new Bot[] {
			new Bot(1, new Position(100, 100), speed, 0),
			new Bot(2, new Position(100, 200), speed, 0),
			new Bot(3, new Position(100, 300), speed, 0),
			new Bot(4, new Position(100, 400), speed, 0),
			new Bot(5, new Position(100, 500), speed, 0),
			new Bot(6, new Position(800, 100), speed, 180),
			new Bot(7, new Position(800, 200), speed, 180),
			new Bot(8, new Position(800, 300), speed, 180),
			new Bot(9, new Position(800, 400), speed, 180),
			new Bot(10, new Position(800, 500), speed, 180),
		};
		isPlayer = new boolean[] {
			true, true, true, true, true,
			false, false, false, false, false,
		};
		isBotDead = new boolean[bots.length];
	}

	public GameWorld(List<Bot> playerBots, List<Bot> enemyBots, GameConstants gameConstants) {
		this.gameConstants = gameConstants;
		bots = new Bot[playerBots.size() + enemyBots.size()];
		isPlayer = new boolean[bots.length];
		isBotDead = new boolean[bots.length];
		int i = 0;
		for (Bot bot: playerBots) {
			bots[i] = bot;
			isPlayer[i++] = true;
		}
		for (Bot bot: enemyBots) {
			bots[i] = bot;
			isPlayer[i++] = false;
		}
	}

	public GameWorld(Bot[] playerBots, Bot[] enemyBots, GameConstants gameConstants) {
		this.gameConstants = gameConstants;
		bots = new Bot[playerBots.length + enemyBots.length];
		isPlayer = new boolean[bots.length];
		isBotDead = new boolean[bots.length];
		for (int i = 0; i < playerBots.length; i++) {
			bots[i] = playerBots[i];
			isPlayer[i] = true;
		}
		for (int i = 0; i < enemyBots.length; i++) {
			bots[i + playerBots.length] = enemyBots[i];
			isPlayer[i + playerBots.length] = false;
		}
	}



	/**
	 * @return All player's living bots.
	 */
	public List<Bot> getPlayerBots() {
		return filterAlive(true);
	}

	/**
	 * @return All player's living bots.
	 */
	public Bot[] getPlayerBotsArr() {
		List<Bot> tmp = getPlayerBots();
		Bot[] ret = new Bot[tmp.size()];
		return tmp.toArray(ret);
	}

	/**
	 * @return All enemy's living bots.
	 */
	public List<Bot> getEnemyBots() {
		return filterAlive(false);
	}

	/**
	 * @return All enemy's living bots.
	 */
	public Bot[] getEnemyBotsArr() {
		List<Bot> tmp = getEnemyBots();
		Bot[] ret = new Bot[tmp.size()];
		return tmp.toArray(ret);
	}

	private List<Bot> filterAlive(boolean player) {
		List<Bot> ret = new ArrayList<Bot>();
		for (int i = 0; i < bots.length; i++) {
			if (!isBotDead[i] && isPlayer[i] == player) {
				ret.add(bots[i]);
			}
		}
		return ret;
	}



	public Queue<GameEvent> collisionEventQueue = new LinkedList<GameEvent>();



	public void commandBots(Map<Bot, BotCommand> commands) {
		for (int i = 0; i < bots.length; i++) {
			if (commands.containsKey(bots[i])) {
				bots[i].applyCommand(commands.get(bots[i]), gameConstants);
			}
		}
	}

	public void commandBot(Bot bot, BotCommand botCommand) {
		for (int i = 0; i < bots.length; i++) {
			if (bots[i].equals(bot)) {
				bot.applyCommand(botCommand, gameConstants);
			}
		}
	}


	/**
	 * Move bots and calculate collision. The step should be small enough to make collision detection possible.
	 */
	public void tick(int stepMilliseconds) {

		for (int i = 0; i < bots.length; i++) {
			float dist = bots[i].speed * stepMilliseconds / 1000.f;
			applyPositionChange(bots[i], dist);
		}

		// don't immediately make bot dead.
		// the reason is that we want to allow interaction between multiple bots at once in some rare cases.
		boolean[] died = new boolean[bots.length];

		for (int i = 0; i < bots.length; ++i) {
			for (int j = i + 1; j < bots.length; ++j) {
				if (isBotDead[i] || isBotDead[j]) continue;
				CollisionType ct = calculateCollision(bots[i], bots[j]);
				if (ct == CollisionType.Destroying) {
					died[j] = true;
					collisionEventQueue.add(new GameEvent(GameEvent.GameEventType.Destruction, bots[i], bots[j]));
				}
				if (ct == CollisionType.Destroyed) {
					died[i] = true;
					collisionEventQueue.add(new GameEvent(GameEvent.GameEventType.Destruction, bots[j], bots[i]));
				}
				if (ct == CollisionType.Bounce) {
					processBounce(bots[i], bots[j]);
					collisionEventQueue.add(new GameEvent(GameEvent.GameEventType.Bounce, bots[i], bots[j]));
				}
			}
		}

		for (int i = 0; i < died.length; i++) {
			if (died[i]) {
				isBotDead[i] = true;
			}
		}
	}



	public enum CollisionType {
		Destroying,
		Destroyed,
		Bounce,
		None,
	}



	private void processBounce(Bot bot1, Bot bot2) {
		float dist = Geometry.sqrt[bot1.position.distanceSqTo(bot2.position)];
		float bounceDist = dist / 2;
		// bots will be oriented back to back after the bounce
		int angle = Geometry.vectorOrientation(bot1.position.x, bot1.position.y,
			bot2.position.x, bot2.position.y);
		bot1.setOrientation(Geometry.normalizeOrientation(angle + 180));
		bot2.setOrientation(angle);
		applyPositionChange(bot1, bounceDist);
		applyPositionChange(bot2, bounceDist);
		bot1.setSpeed(gameConstants.speedLevels[1].speed);
		bot2.setSpeed(gameConstants.speedLevels[1].speed);
	}



	private Position calculatePositionAfterDistance(Bot bot, float dist) {
		return Geometry.projectPoint(bot.position.x, bot.position.y, bot.orientation, dist);
	}



	/**
	 * Gets position of the foremost part of the bumper.
	 */
	private Position getNosePosition(Bot bot) {
		return calculatePositionAfterDistance(bot, gameConstants.botRadius);
	}

	private float noseCheckDistFactor = 0.76536686473f; // magic

	// Destroying: bot1 destroys bot2
	// Destroyed: bot2 destroys bot1
	// Bounce and None: guess what does that mean... duh
	private CollisionType calculateCollision(Bot bot1, Bot bot2) {
		boolean haveIntersection = bot1.position.distanceSqTo(bot2.position) < 4 * gameConstants.botRadius * gameConstants.botRadius;
		if (haveIntersection) {
			float noseCheckDistSq = noseCheckDistFactor * gameConstants.botRadius * noseCheckDistFactor * gameConstants.botRadius;
			Position middle = new Position((bot1.position.x + bot2.position.x) / 2, (bot1.position.y + bot2.position.y) / 2);
			boolean isBumper1In2 = middle.distanceSqTo(getNosePosition(bot1)) < noseCheckDistSq;
			boolean isBumper2In1 = middle.distanceSqTo(getNosePosition(bot2)) < noseCheckDistSq;
			if (isBumper1In2 && !isBumper2In1) return CollisionType.Destroying;
			if (!isBumper1In2 && isBumper2In1) return CollisionType.Destroyed;
			return CollisionType.Bounce;
		} else {
			return CollisionType.None;
		}
	}



	/**
	 * Move bot accordingly to its speed and orientation.
	 * Should be used only with small time intervals.
	 */
	public void applyPositionChange(Bot bot, float distance) {

		Position position = bot.position;
		int orientation = bot.orientation;
		int r = gameConstants.botRadius;

		// cannot move against wall (or slide along it - assumption made when observing matches on server)
		if ((Geometry.isOrientedUp(orientation) && position.y - r <= 0)
			|| (Geometry.isOrientedDown(orientation) && position.y + r >= gameConstants.worldHeight - 1)
			|| (Geometry.isOrientedLeft(orientation) && position.x - r <= 0)
			|| (Geometry.isOrientedRight(orientation) && position.x + r >= gameConstants.worldWidth - 1)) {
			bot.setSpeed(gameConstants.speedLevels[1].speed);
			return;
		}

		Position newPosition = calculatePositionAfterDistance(bot, distance);
		// not perfect - we may end up behind wall and then teleport back in horizontal or vertical direction
		// but with reasonable time intervals it won't be more than pixel or two
		newPosition.x = Math.max(r, Math.min(gameConstants.worldWidth - r - 1, newPosition.x));
		newPosition.y = Math.max(r, Math.min(gameConstants.worldHeight - r - 1, newPosition.y));
		bot.setPosition(newPosition);
	}
}
