package sk.Aries512.BotWarz.Game;

public enum GameResult {
	GameWon,
	GameLost,
	Draw,
}
