package sk.Aries512.BotWarz.Game.Ai;

import sk.Aries512.BotWarz.Game.Bot;
import sk.Aries512.BotWarz.Game.BotCommand;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Used for testing.
 */
public class RandomAi extends Ai {

	private static Random rnd = new Random(System.currentTimeMillis());

	@Override
	public Map<Bot, BotCommand> calculateNextTurn(List<Bot> playerBots, List<Bot> enemyBots,
												  int simulationDuration, int millisUntilDispatch) {
		Map<Bot, BotCommand> result = new HashMap<Bot, BotCommand>();
		playerBots.forEach(bot -> {
			BotCommand command = null;
			int angle = gameConstants.speedLevels[gameConstants.getSpeedLevelBySpeed(bot.speed)].maxAngle;
			// 76% chance to do nothing, 8% to accelerate/decelerate and 4% to turn
			int choice = rnd.nextInt(25);
			switch (choice) {
				case 19:
				case 20:
					command = new BotCommand(BotCommand.BotCommandType.Accelerate);
					break;
				case 21:
				case 22:
					command = new BotCommand(BotCommand.BotCommandType.Decelerate);
					break;
				case 23:
					command = new BotCommand(BotCommand.BotCommandType.Turn, angle);
					break;
				case 24:
					command = new BotCommand(BotCommand.BotCommandType.Turn, -angle);
					break;
			}
			if (command != null) {
				result.put(bot, command);
			}
		});
		return result;
	}

	@Override
	public int getExpectedTurnCalculationDuration(List<Bot> playerBots, List<Bot> enemyBots) {
		return 0;
	}
}
