package sk.Aries512.BotWarz.Game.Ai;

import sk.Aries512.BotWarz.Game.*;
import sk.Aries512.BotWarz.Utils.Geometry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Rather bruteforce AI, deciding the best bot commands by trying (almost) all the possible command combinations
 * and evaluating their quality. Evaluation has two phases:
 * - simulation phase, where the bots are moving and we evaluate how well they did
 * - future phase, which evaluates quality of the positions of the bots
 */
public class TurnEvalAi extends Ai {

	// this needs to be calculated quickly, so the code is sometimes ugly to be faster
	// we prefer arrays to collections, integers to decimals, floats to doubles, etc.



	private static final int SIMULATION_STEP = 10;

	private long firstTurnTime = -1;
	private int iterationNumber = 0;

	/**
	 * Expected time of turn calculation based on the number of remaining bots in game.
	 * Calculated for i5 @ 2.5GHz and sometimes can be pretty inaccurate.
	 */
	private static final int[] EXPECTED_CALCULATION_TIMES = new int[] {
		0,
		5, 5, 5, 5, 10,
		15, 25, 45, 75, 120,
	};

	@Override
	public int getExpectedTurnCalculationDuration(List<Bot> playerBots, List<Bot> enemyBots) {
		return EXPECTED_CALCULATION_TIMES[(playerBots == null ? 0 : playerBots.size()) + (enemyBots == null ? 0 : enemyBots.size())];
	}



	public TurnEvalAi(GameConstants gameConstants) {
		this.gameConstants = gameConstants;
	}



	@Override
	public Map<Bot, BotCommand> calculateNextTurn(List<Bot> playerBots, List<Bot> enemyBots,
												  int simulationDuration, int millisUntilDispatch) {

		Map<Bot, BotCommand> result = new HashMap<Bot, BotCommand>();

		if (playerBots == null || enemyBots == null || playerBots.size() == 0 || enemyBots.size() == 0) {
			return result;
		}

		iterationNumber++;
		long timeStarted = System.currentTimeMillis();
		if (firstTurnTime < 0) {
			firstTurnTime = timeStarted;
		}

		// just accelerate at the start
		if (iterationNumber < 4 && timeStarted - firstTurnTime < 650) {
			playerBots.forEach(bot -> result.put(bot, new BotCommand(BotCommand.BotCommandType.Accelerate)));
			return result;
		}

		playerBots.forEach(bot -> isIdPlayer[bot.id] = true);

		// move bots at the beginning to (approximately) compensate for the delay that will appear between
		// calculation start and sending result of this calculation to the server.
		// we pretend that enemy won't move his bots during that time (there is nothing better to do anyway)

		List<Bot> playerBotsCopy = playerBots.stream().map(Bot::new).collect(Collectors.toList());
		List<Bot> enemyBotsCopy = enemyBots.stream().map(Bot::new).collect(Collectors.toList());
		GameWorld gw = new GameWorld(playerBotsCopy, enemyBotsCopy, gameConstants);
		for (int i = 0; i < millisUntilDispatch; i += SIMULATION_STEP) {
			gw.tick(SIMULATION_STEP);
		}
		Bot[] pBots = gw.getPlayerBotsArr();
		Bot[] eBots = gw.getEnemyBotsArr();

		return findBestCommands(pBots, eBots, Math.max(1, simulationDuration - millisUntilDispatch));
	}



	private Map<Bot, BotCommand> findBestCommands(Bot[] playerBots, Bot[] enemyBots, int simulationPhaseDuration) {

		long timeStarted = System.currentTimeMillis();

		// generate all meaningful moves
		List<BotCommand>[] commands = new ArrayList[playerBots.length];
		for (int i = 0; i < playerBots.length; i++) {
			commands[i] = generateMoves(playerBots[i]);
		}

		// stores best score of the moves evaluation, must be reset before
		bestScore = Integer.MIN_VALUE;
		// find the best move
		// run simulation step for at least once to identify collisions
		evaluateCommands(playerBots, commands, enemyBots, simulationPhaseDuration, new BotCommand[playerBots.length], 0);


		System.out.println("Iter. " + iterationNumber
			+ ", bot cnt: " + (playerBots.length + enemyBots.length)
			+ ", calculation duration: " + (System.currentTimeMillis() - timeStarted));


		Map<Bot, BotCommand> result = new HashMap<Bot, BotCommand>();
		for (int i = 0; i < playerBots.length; i++) {
			//System.out.println(playerBots[i] + ": " + bestCommands[i]);
			if (bestCommands[i].type != BotCommand.BotCommandType.Noop) {
				result.put(playerBots[i], bestCommands[i]);
			}
		}
		return result;
	}



	// TODO: this is ugly
	private boolean[] isIdPlayer = new boolean[1000];



	/**
	 * Create all possible meaningful moves for the bot.
	 */
	private List<BotCommand> generateMoves(Bot bot) {

		List<BotCommand> commands = new ArrayList<BotCommand>();
		Bot tester = new Bot(bot);

		int speedLevel = gameConstants.getSpeedLevelBySpeed(bot.speed);
		int orientationDelta = gameConstants.speedLevels[speedLevel].maxAngle;

		// damn, we are stuck, let's try fix it
		int unstuckOrientation = getUnstuckOrientation(bot);
		if (unstuckOrientation != 0) {
			commands.add(new BotCommand(BotCommand.BotCommandType.Turn, unstuckOrientation * orientationDelta));
			return commands;
		}

		// don't use slowest speed at all, it's lame
		// otherwise try to decelerate
		if (speedLevel > 2) {
			commands.add(new BotCommand(BotCommand.BotCommandType.Decelerate));
		}

		// do nothing at all
		if (isWorthTrying(tester)) {
			commands.add(new BotCommand(BotCommand.BotCommandType.Noop));
		}

		// try to accelerate (if we can)
		if (speedLevel < gameConstants.speedLevels.length - 1) {
			tester.setSpeed(gameConstants.speedLevels[speedLevel + 1].speed);
			if (isWorthTrying(tester)) {
				commands.add(new BotCommand(BotCommand.BotCommandType.Accelerate));
			}
		}

		// for speeds 2-5 we test only maximum turning angles, it should be enough
		// for speed 1, test max turning angle and its half too
		tester.setSpeed(gameConstants.speedLevels[speedLevel].speed);
		// right turn
		tester.setOrientation(bot.orientation + orientationDelta);
		if (isWorthTrying(tester)) {
			commands.add(new BotCommand(BotCommand.BotCommandType.Turn, orientationDelta));
		}
		// left turn
		tester.setOrientation(bot.orientation - orientationDelta);
		if (isWorthTrying(tester)) {
			commands.add(new BotCommand(BotCommand.BotCommandType.Turn, -orientationDelta));
		}
		// test halves
		if (speedLevel == 1) {
			tester.setOrientation(bot.orientation + orientationDelta / 2);
			if (isWorthTrying(tester)) {
				commands.add(new BotCommand(BotCommand.BotCommandType.Turn, orientationDelta / 2));
			}
			tester.setOrientation(bot.orientation - orientationDelta / 2);
			if (isWorthTrying(tester)) {
				commands.add(new BotCommand(BotCommand.BotCommandType.Turn, -orientationDelta / 2));
			}
		}

		return commands;
	}



	/**
	 * Finds the orientation that helps bot to unstuck from the border.
	 * @return -1 to turn left, 1 to turn right, 0 in case it is not stuck.
	 */
	private int getUnstuckOrientation(Bot bot) {
		int r = gameConstants.botRadius;
		float y = bot.position.y + (Geometry.isOrientedDown(bot.orientation) ? r : -r);
		float x = bot.position.x + (Geometry.isOrientedRight(bot.orientation) ? r : -r);
		int dU = Geometry.distFromHorizontalLine(y, bot.orientation, 0);
		int dD = Geometry.distFromHorizontalLine(y, bot.orientation, gameConstants.worldHeight - 1);
		int dL = Geometry.distFromVerticalLine(x, bot.orientation, 0);
		int dR = Geometry.distFromVerticalLine(x, bot.orientation, gameConstants.worldWidth - 1);
		int cnt = b2i(dU <= 1) + b2i(dD <= 1) + b2i(dL <= 1) + b2i(dR <= 1);
		// if we are in the corner, always return the same response to avoid oscillations
		if (cnt > 1) return 1;
		// find correct orientation to unstuck
		if (dU <= 1) {
			return bot.orientation < -90 ? -1 : 1;
		}
		if (dD <= 1) {
			return bot.orientation < 90 ? -1 : 1;
		}
		if (dL <= 1) {
			return bot.orientation > 0 ? -1 : 1;
		}
		if (dR <= 1) {
			return bot.orientation < 0 ? -1 : 1;
		}
		return 0;
	}

	private int b2i(boolean bool) {
		return bool ? 1 : 0;
	}



	/**
	 * Checks if the bot state can lead into something useful.
	 * Currently based only on the fact whether we are at the safe distance from the wall.
	 */
	private boolean isWorthTrying(Bot bot) {
		int dist = getBorderDist(bot);
		int start = 20;
		int end = 100;
		if (dist <= start) {
			return bot.speed <= start;
		}
		if (dist > end) {
			return true;
		}
		return dist > distanceNeededForSpeed[bot.speed];
	}

	private int getBorderDist(Bot bot) {
		return Geometry.distanceUntilBorder(
			bot.position.x, bot.position.y, bot.orientation,
			gameConstants.botRadius,
			0, gameConstants.worldWidth - 1,
			0, gameConstants.worldHeight - 1);
	}



	// using global variables instead for the recursive function instead of return value
	// I know, ugly, but it has to be done quickly
	private int bestScore = Integer.MIN_VALUE;
	private BotCommand[] bestCommands = null;

	/**
	 * Finds the best move (pairing bot-command) and stores it in the bestCommands array.
	 * @param playerBots Our bots.
	 * @param commands All possible commands for our bots.
	 * @param enemyBots Enemy bots.
	 * @param simulationDuration Duration of the simulation which is evaluated.
	 * @param currentCommands Command mapping being that is just being evaluated.
	 * @param position Depth of the recursive function.
	 */
	private void evaluateCommands(Bot[] playerBots, List<BotCommand>[] commands, Bot[] enemyBots, int simulationDuration, BotCommand[] currentCommands, int position) {
		for (BotCommand command: commands[position]) {
			currentCommands[position] = command;
			// populating last command, we can now evaluate this configuration
			if (position == playerBots.length - 1) {
				// we will modify bots in eval function, so the copy has to be done
				Bot[] playerBotsCopy = new Bot[playerBots.length];
				Bot[] enemyBotsCopy = new Bot[enemyBots.length];
				for (int i = 0; i < playerBots.length; i++) {
					playerBotsCopy[i] = new Bot(playerBots[i]);
				}
				for (int i = 0; i < enemyBots.length; i++) {
					enemyBotsCopy[i] = new Bot(enemyBots[i]);
				}
				int value = evaluateState(playerBotsCopy, enemyBotsCopy, currentCommands, simulationDuration);
				if (value > bestScore) {
					bestScore = value;
					bestCommands = currentCommands.clone();
				}
			} else {
				// just keep generating next configuration
				evaluateCommands(playerBots, commands, enemyBots, simulationDuration, currentCommands, position + 1);
			}
		}
	}



	private int evaluateState(Bot[] playerBots, Bot[] enemyBots, BotCommand[] playerCommands, int simulationDuration) {

		GameWorld gw = new GameWorld(playerBots, enemyBots, gameConstants);
		for (int i = 0; i < playerBots.length; i++) {
			gw.commandBot(playerBots[i], playerCommands[i]);
		}
		int scoreSimulation = evalSimulationPhase(gw, simulationDuration);

		Bot[] playerBotsAfterSimulation = gw.getPlayerBotsArr();
		Bot[] enemyBotsAfterSimulation = gw.getEnemyBotsArr();
		int scoreFuture = evalFuturePhase(playerBotsAfterSimulation, enemyBotsAfterSimulation);

		return scoreSimulation + scoreFuture;
	}



	/**
	 * Simulates the game for a given duration and evaluates collisions.
	 */
	private int evalSimulationPhase(GameWorld gameWorld, int simulationDuration) {
		for (int t = 0; t < simulationDuration; t += SIMULATION_STEP) {
			gameWorld.tick(SIMULATION_STEP);
		}
		int score = 0;
		while (!gameWorld.collisionEventQueue.isEmpty()) {
			GameEvent event = gameWorld.collisionEventQueue.poll();
			boolean isSourcePlayer = isIdPlayer[event.source.id];
			boolean isTargetPlayer = isIdPlayer[event.target.id];
			if (event.type == GameEvent.GameEventType.Destruction) {
				if (isSourcePlayer && !isTargetPlayer) score += 250;
				if (!isSourcePlayer && isTargetPlayer) score -= 250;
				if (isSourcePlayer && isTargetPlayer) score -= 300;
			}
			if (event.type == GameEvent.GameEventType.Bounce) {
				if (isSourcePlayer && !isTargetPlayer) score -= 50;
				if (!isSourcePlayer && isTargetPlayer) score -= 50;
				if (isSourcePlayer && isTargetPlayer) score -= 80;
			}
		}
		return score;
	}



	// scores for the speed: too slow = bad, faster = better, but not too fast as it is easy to miss enemies
	private static final int[] speedScores = new int[] { 0, -20, -10, 0, 20, 25};

	// try to pursue enemies more intensively of only one or two are remaining
	private static final int[] pursuitWeights = new int[] { 0, 5, 2, 1, 1, 1};

	/**
	 * Evaluates game state after the moving phase has ended.
	 */
	private int evalFuturePhase(Bot[] playerBots, Bot[] enemyBots) {

		int score = 0;

		for (int i = 0; i < playerBots.length; i++) {

			Bot bot = playerBots[i];

			score += speedScores[gameConstants.getSpeedLevelBySpeed(bot.speed)];
			// we don't want to stay close to borders or even get stuck there
			score += borderClosenessPenalty(bot);

			// check the relative position of the enemy bots.
			// behind us = bad, behind us and following us = very bad
			// ahead of us = good, ahead of us and turning their back to us = attaaaaaaack!
			// TODO: add bot orientation weighing
			for (int j = 0; j < enemyBots.length; j++) {
				Bot eBot = enemyBots[j];
				int anglePE = Geometry.angleBetween(
					bot.position.x, bot.position.y, bot.orientation,
					eBot.position.x, eBot.position.y);
				int angleEP = Geometry.angleBetween(
					eBot.position.x, eBot.position.y, eBot.orientation,
					bot.position.x, bot.position.y);
				int dist = Geometry.sqrt[bot.position.distanceSqTo(eBot.position)] - gameConstants.botRadius * 2;
				if (dist < 0) dist = 0;
				// check bots ahead of us
				if (anglePE < 90) {
					score += pursuitWeights[enemyBots.length] * botDistanceWeight[dist] * botAngleWeight[anglePE];
				}
				if (angleEP < 90) {
					//score -= botDistanceWeight[dist] * botAngleWeight[angleEP];
				}
			}

			// check the relative position of our own bots.
			// in general, we want to spread a bit.
			for (int j = 0; j < playerBots.length; j++) {
				if (i != j) {
					int angle = Geometry.angleBetween(
						bot.position.x, bot.position.y, bot.orientation,
						playerBots[j].position.x, playerBots[j].position.y);
					int dist = Geometry.sqrt[bot.position.distanceSqTo(playerBots[j].position)] - gameConstants.botRadius * 2;
					if (dist < 0) dist = 0;
					if (angle < 90) {
						score -= 0.75 * botDistanceWeight[dist] * botAngleWeight[angle];
					}
					// don't add it to the score two times
					if (i < j) {
						score -= dist < gameConstants.botRadius ? spreadDistanceWeight[dist] : 0;
					}
				}
			}
		}

		return score;
	}



	// total range: -pi*10 .. 0
	private int borderClosenessPenalty(Bot bot) {
		int orient = bot.orientation;
		float direct = borderClosenessWeight[getBorderDist(bot)];
		bot.setOrientation(orient - 90);
		float side1 = borderClosenessWeight[getBorderDist(bot)];
		bot.setOrientation(orient + 90);
		float side2 = borderClosenessWeight[getBorderDist(bot)];
		bot.setOrientation(orient);
		return (int)(direct * 6 + side1 * 2 + side2 * 2);
	}



	private static final int[] distanceNeededForSpeed = new int[2000];
	private static final float[] borderClosenessWeight = new float[2000];
	private static final float[] botDistanceWeight = new float[2000];
	private static final float[] botAngleWeight = new float[400];
	private static final float[] spreadDistanceWeight = new float[2000];



	// precalculated stuff
	static {
		for (int x = 0; x < distanceNeededForSpeed.length; x++) {
			distanceNeededForSpeed[x] = (int)(0.22 * x + 16);
		}
		for (int x = 0; x < borderClosenessWeight.length; x++) {
			borderClosenessWeight[x] = (float)(Math.atan(x / 10) - Math.PI / 2);
		}
		for (int x = 0; x < botDistanceWeight.length; x++) {
			botDistanceWeight[x] = 1.f / (0.001f * (x + 50));
		}
		for (int x = 0; x < botAngleWeight.length; x++) {
			botAngleWeight[x] = (-x / 45.f) + 2;
		}
		for (int x = 0; x < spreadDistanceWeight.length; x++) {
			spreadDistanceWeight[x] = x < 6 ? 100 : (5.f / (0.25f + x * 0.05f));
		}
	}

}
