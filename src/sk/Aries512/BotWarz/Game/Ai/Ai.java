package sk.Aries512.BotWarz.Game.Ai;

import sk.Aries512.BotWarz.Game.Bot;
import sk.Aries512.BotWarz.Game.BotCommand;
import sk.Aries512.BotWarz.Game.GameConstants;
import java.util.List;
import java.util.Map;

public abstract class Ai {

	protected GameConstants gameConstants;

	public Ai() {
		this.gameConstants = new GameConstants();
	}

	public Ai(GameConstants gameconstants) {
		this.gameConstants = gameconstants;
	}

	public void setGameConstants(GameConstants gameConstants) {
		this.gameConstants = gameConstants;
	}

	/**
	 * Calculates best actions for each of the player bots that play against enemy bots.
	 * @return Returns commands mapped to playerBots. Notice that some of the bots don't have to have
	 * any command mapped.
	 */
	public abstract Map<Bot, BotCommand> calculateNextTurn(List<Bot> playerBots, List<Bot> enemyBots,
														   int simulationDuration, int millisUntilDispatch);

	public abstract int getExpectedTurnCalculationDuration(List<Bot> playerBots, List<Bot> enemyBots);

}
