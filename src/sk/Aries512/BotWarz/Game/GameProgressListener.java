package sk.Aries512.BotWarz.Game;

import sk.Aries512.BotWarz.Server.ErrorType;
import java.util.List;

public interface GameProgressListener {

	public void onGameStart(long time, GameConstants gameConstants, List<Bot> playerBots, List<Bot> enemyBots);

	public void onGameProgress(long time, String lastCmdId, List<Bot> playerBots, List<Bot> enemyBots);

	public void onGameEnd(long time, GameResult result, List<Bot> remainingBots);

	public void onGameTermination(long time, String message);

	public void onCommandError(long time, ErrorType error, String message);

}
