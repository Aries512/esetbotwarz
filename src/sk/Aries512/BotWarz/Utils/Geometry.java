package sk.Aries512.BotWarz.Utils;

import static java.lang.Math.*;
import sk.Aries512.BotWarz.Game.Position;

public final class Geometry {

	private Geometry() {
	}

	// using only primitives as arguments to save some time... class will be used in intensive calculations
	// preferring integers to decimals and floats to doubles to make it a bit faster, because precision isn't really needed

	/**
	 * Stores pre-calculated square roots.
	 * Let's hope we won't need more than sqrt(2000^2).
	 */
	public static final int[] sqrt = new int[2000 * 2000];

	static {
		for (int x = 0; x < sqrt.length; x++) {
			sqrt[x] = (int) round(Math.sqrt(x));
		}
	}



	public static int distSq(float x1, float y1, float x2, float y2) {
		return (int)((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	}



	public static boolean isOrientedUp(int orientation) {
		return orientation < 0 && orientation > -180;
	}
	public static boolean isOrientedDown(int orientation) {
		return orientation > 0 && orientation < 180;
	}
	public static boolean isOrientedLeft(int orientation) {
		return orientation > 90 || orientation < -90;
	}
	public static boolean isOrientedRight(int orientation) {
		return abs(orientation) < 90;
	}

	/**
	 * Orientation should be represented accordingly to Eset format, that is:
	 * 0 = facing east, positive angles clockwise, negative counterclockwise,
	 * with maximum/minimum 180/-180 when facing west.
	 */
	public static int normalizeOrientation(int orientation) {
		orientation %= 360;
		if (orientation > 180) {
			return orientation - 360;
		}
		if (orientation < -180) {
			return 360 + orientation;
		}
		return orientation;
	}

	private static double orientationToRadians(int orientation) {
		return toRadians((orientation + 360) % 360);
	}



	/**
	 * Find point's position after moving it by dist following a given orientation.
	 */
	public static Position projectPoint(float x, float y, int orientation, float dist) {
		double alpha = orientationToRadians(orientation);
		return new Position((float)(x + dist * cos(alpha)), (float)(y + dist * sin(alpha)));
	}



	public static int distFromVerticalLine(float x, int orientation, float lineX) {
		double alpha = orientationToRadians(orientation);
		if (alpha == 90 || alpha == -90) return lineX == x ? 0 : Integer.MAX_VALUE;
		if (isOrientedLeft(orientation) && x < lineX) return Integer.MAX_VALUE;
		if (isOrientedRight(orientation) && x > lineX) return Integer.MAX_VALUE;
		return (int)abs(abs(x - lineX) / cos(alpha));
	}

	public static int distFromHorizontalLine(float y, int orientation, float lineY) {
		double alpha = orientationToRadians(orientation);
		if (alpha == 0 || alpha == 180 || alpha == -180) return lineY == y ? 0 : Integer.MAX_VALUE;
		if (isOrientedUp(orientation) && y < lineY) return Integer.MAX_VALUE;
		if (isOrientedDown(orientation) && y > lineY) return Integer.MAX_VALUE;
		return (int)abs(abs(y - lineY) / sin(alpha));
	}



	public static int vectorOrientation(float ax, float ay, float bx, float by) {
		float dx = bx - ax;
		float dy = by - ay;
		int alpha = (int)toDegrees(atan(dy / dx));
		if (dx < 0) {
			if (dy < 0) {
				alpha -= 180;
			} else {
				alpha += 180;
			}
		}
		return alpha;
	}



	/**
	 * Angle in degrees between vector starting at (x,y) having given orientation and the vector [(x,y),(ox,oy)].
	 */
	public static int angleBetween(float x, float y, int orientation, float ox, float oy) {
		Position p = projectPoint(x, y, orientation, 10);
		float ux = p.x - x;
		float uy = p.y - y;
		float vx = ox - x;
		float vy = oy - y;
		return (int) toDegrees(
			acos((ux * vx + uy * vy) / (sqrt[(int) (ux * ux + uy * uy)] * sqrt[(int) (vx * vx + vy * vy)])));
	}



	// TODO: test and document
	public static int distanceUntilBorder(float x, float y, int orientation, int radius, int xMin, int xMax, int yMin, int yMax) {
		float yy = y + (Geometry.isOrientedDown(orientation) ? radius : -radius);
		// Eset's implementation seems to differ a bit from mine, so we better care for unexpected values
		if (yy < yMin) yy = 0;
		if (yy > yMax) yy = yMax;
		float xx = x + (Geometry.isOrientedRight(orientation) ? radius : -radius);
		if (xx < xMin) xx = xMin;
		if (xx > xMax) xx = xMax;
		int d1 = Geometry.distFromHorizontalLine(yy, orientation, yMin);
		int d2 = Geometry.distFromHorizontalLine(yy, orientation, yMax);
		int d3 = Geometry.distFromVerticalLine(xx, orientation, xMin);
		int d4 = Geometry.distFromVerticalLine(xx, orientation, xMax);
		int d = min (min(d1, d2), min(d3, d4));
		return d == Integer.MAX_VALUE ? 0 : d;
	}

}
