package sk.Aries512.BotWarz.Server;

import sk.Aries512.BotWarz.Game.Bot;
import sk.Aries512.BotWarz.Game.BotCommand;
import sk.Aries512.BotWarz.Game.GameProgressListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Server {

	public int getDelayBetweenCommands() {
		return 250;
	}

	public abstract void initialize() throws GameServerException;
	public abstract void dispatchCommands(Map<Bot, BotCommand> commands) throws GameServerException;

	protected List<GameProgressListener> listeners = new ArrayList<GameProgressListener>();

	public void subscribeGameProgressListener(GameProgressListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	public boolean unsubscribeGameProgressListener(GameProgressListener listener) {
		return listeners.remove(listener);
	}

}
