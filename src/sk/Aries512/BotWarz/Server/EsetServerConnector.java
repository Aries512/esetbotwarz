package sk.Aries512.BotWarz.Server;

import sk.Aries512.BotWarz.Game.Bot;
import sk.Aries512.BotWarz.Game.BotCommand;
import sk.Aries512.BotWarz.Game.GameProgressListener;
import sk.Aries512.BotWarz.Game.GameResult;
import java.io.*;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Map;

public class EsetServerConnector extends Server {

	public int getDelayBetweenCommands() {
		return 215; // Eset-given delay + few milliseconds to be on the safe side
	}

	private String host;
	private int port;
	private String nickname;
	private String token;
	private RemoteServerProtocol protocol;
	private Socket socket = null;
	private BufferedReader inFromServer;
	private OutputStream outToServer;
	private boolean isInitialized = false;

	public EsetServerConnector(String host, int port, String nickname, String token, RemoteServerProtocol protocol) {
		this.host = host;
		this.port = port;
		this.nickname = nickname;
		this.token = token;
		this.protocol = protocol;
	}

	@Override
	public void initialize() throws GameServerException {
		if (isInitialized) throw new IllegalStateException("Already initialized!");
		try {
			socket = new Socket(host, port);
			socket.setTrafficClass(0x10);
			socket.setTcpNoDelay(true);
			socket.setSendBufferSize(8);
			outToServer = socket.getOutputStream();
			inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String response = inFromServer.readLine();
			Message msg = protocol.decode(response);
			if (msg.type == Message.MessageType.LoginPrompt) {
				login(msg.randomStr);
			} else {
				closeConnections();
				throw new GameServerException("Server did not respond with the correct login prompt.", null);
			}
			isInitialized = true;
			new Thread(() -> waitForData()).start();
		} catch (IOException e) {
			closeConnections();
			throw new GameServerException("Connection error!", e);
		}
	}



	private void closeConnections() {
		try {
			inFromServer.close();
			outToServer.close();
			socket.close();
		} catch (IOException e) {
		}
	}



	private boolean login(String key) throws IOException {
		String login = protocol.createLoginMessage(nickname, getSHA1(key + token));
		outToServer.write(login.getBytes());
		String response = inFromServer.readLine();
		Message msg = protocol.decode(response);
		return msg.type == Message.MessageType.LoginAnswer;
	}

	private String getSHA1(String str) {
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			// this really should work
		}
		digest.reset();
		digest.update(str.getBytes());
		byte[] data = digest.digest();
		return bytesToString(data);
	}

	private String bytesToString(byte[] data) {
		StringBuilder sb = new StringBuilder(data.length);
		for (int i = 0; i < data.length; i++) {
			sb.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}



	private void waitForData() {
		while (true) {
			String response = null;
			try {
				response = inFromServer.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				closeConnections();
				return;
			}
			Message msg = protocol.decode(response);
			for (GameProgressListener listener: new ArrayList<GameProgressListener>(listeners)) {
				switch (msg.type) {
					case GameStart:
						listener.onGameStart(msg.time, msg.gameConstants, msg.playerBots, msg.enemyBots);
						break;
					case GameUpdate:
						listener.onGameProgress(msg.time, msg.lastCmdId, msg.playerBots, msg.enemyBots);
						break;
					case GameResult:
						// TODO: Eset does not specify what happens in case of draw
						boolean playerWon = msg.winnerNickname.equals(nickname);
						GameResult result = playerWon ? GameResult.GameWon : GameResult.GameLost;
						listener.onGameEnd(msg.time, result, playerWon ? msg.playerBots : msg.enemyBots);
						break;
					case TimeError:
						listener.onCommandError(msg.time, ErrorType.CommandDelayError, msg.additionalInfo);
						break;
					case TerminationError:
						listener.onGameTermination(msg.time, msg.additionalInfo);
						break;
				}
			}
		}
	}



	@Override
	public void dispatchCommands(Map<Bot, BotCommand> commands) throws GameServerException {
		try {
			outToServer.write(protocol.createMessageBotUpdate(commands, "").getBytes());
			outToServer.flush();
		} catch (IOException e) {
			throw new GameServerException("Cannot send data!", e);
		}
	}
}
