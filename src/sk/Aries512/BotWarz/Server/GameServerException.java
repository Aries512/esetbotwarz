package sk.Aries512.BotWarz.Server;

public class GameServerException extends Exception {

	public GameServerException(String message, Throwable cause) {
		super(message, cause);
	}

}
