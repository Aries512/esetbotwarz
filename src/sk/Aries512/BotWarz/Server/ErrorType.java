package sk.Aries512.BotWarz.Server;

public enum ErrorType {
	CommandParsingError,
	CommandDelayError,
	ConnectionError,
	OtherError,
}
