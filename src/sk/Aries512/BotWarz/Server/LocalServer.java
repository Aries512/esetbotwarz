package sk.Aries512.BotWarz.Server;

import sk.Aries512.BotWarz.Game.Ai.Ai;
import sk.Aries512.BotWarz.Game.*;
import java.util.*;

/**
 * Runs the game locally (probably not exactly the same as the Eset implementation and game rules).
 * Used for local testing and visualisation.
 */
public class LocalServer extends Server {

	private GameConstants gameConstants;
	private GameWorld gameWorld;
	private Ai enemyAi;
	private Queue<Map<Bot, BotCommand>> commandQueue;
	private Object commandQueueLockObj = new Object();
	private boolean isInitialized = false;


	public LocalServer(GameConstants gameConstants, Ai enemyAi) {
		this.gameConstants = gameConstants;
		gameWorld = new GameWorld(gameConstants);
		this.enemyAi = enemyAi;
		commandQueue = new LinkedList<Map<Bot, BotCommand>>();
	}

	@Override
	public void initialize() {
		if (isInitialized) throw new IllegalStateException("Already initialized!");
		isInitialized = true;
		lastMoveTime = System.currentTimeMillis();
		new Thread(new Server()).start();
		for (GameProgressListener listener: listeners) {
			listener.onGameStart(lastMoveTime, gameConstants, gameWorld.getPlayerBots(), gameWorld.getEnemyBots());
		}
	}

	@Override
	public void dispatchCommands(Map<Bot, BotCommand> commands) {
		synchronized (commandQueueLockObj) {
			commandQueue.add(commands);
		}
	}



	private static final int UPDATE_PERIOD = 5;
	private static final int COMMAND_DISPATCH_PERIOD = 100;
	private long lastMoveTime = 0;
	private long lastSleepTime = 0;
	private long lastCommandDispatchTime = 0;
	private int lastCommandId = 0;

	private class Server implements Runnable {
		@Override
		public void run() {

			lastSleepTime = System.currentTimeMillis();

			while (true) {

				long lastCycleDuration = System.currentTimeMillis() - lastSleepTime;
				try {
					Thread.sleep(Math.max(1, UPDATE_PERIOD - lastCycleDuration));
				} catch (InterruptedException e) {
				}

				long currTime = System.currentTimeMillis();
				lastSleepTime = currTime;

				if (currTime - lastCommandDispatchTime > COMMAND_DISPATCH_PERIOD) {
					gameWorld.commandBots(enemyAi.calculateNextTurn(gameWorld.getEnemyBots(), gameWorld.getPlayerBots(), 0, 0));
					lastCommandDispatchTime = System.currentTimeMillis();
				}

				synchronized (commandQueueLockObj) {
					while (!commandQueue.isEmpty()) {
						gameWorld.commandBots(commandQueue.poll());
					}
				}

				long timeDelta = System.currentTimeMillis() - lastMoveTime;
				gameWorld.tick((int)timeDelta);
				lastMoveTime = System.currentTimeMillis();

				GameResult result = null;
				List<Bot> playerBots = gameWorld.getPlayerBots();
				List<Bot> enemyBots = gameWorld.getEnemyBots();
				if (playerBots.isEmpty()) {
					result = enemyBots.isEmpty() ? GameResult.Draw : GameResult.GameLost;
				} else if (enemyBots.isEmpty()) {
					result = GameResult.GameWon;
				}

				for (GameProgressListener listener: new ArrayList<GameProgressListener>(listeners)) {
					if (result == null) {
						listener.onGameProgress(
							lastMoveTime,
							Integer.toString(lastCommandId),
							playerBots,
							enemyBots);
					} else {
						List<Bot> bots = new ArrayList<Bot>(playerBots);
						bots.addAll(enemyBots);
						listener.onGameEnd(lastMoveTime, result, bots);
						return;
					}
				}

				lastCommandId++;

			}
		}
	}

}
