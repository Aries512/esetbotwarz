package sk.Aries512.BotWarz.Server;

import sk.Aries512.BotWarz.Game.Bot;
import sk.Aries512.BotWarz.Game.GameConstants;
import java.util.List;

/**
 * General purpose class holding data received from the server.
 */
public class Message {

	public enum MessageType {
		LoginPrompt,
		LoginAnswer,
		GameStart,
		GameUpdate,
		GameResult,
		TerminationError,
		TimeError,
		Other,
	}

	public MessageType type = MessageType.Other;
	public String additionalInfo;
	public String randomStr;
	public List<Bot> playerBots;
	public List<Bot> enemyBots;
	public String lastCmdId;
	public long time;
	public String winnerNickname;
	public GameConstants gameConstants;

	public Message() {
		this.type = MessageType.Other;
		this.gameConstants = new GameConstants();
	}

	public Message(MessageType type) {
		this.type = type;
		this.gameConstants = new GameConstants();
	}

}
