package sk.Aries512.BotWarz.Server;

import sk.Aries512.BotWarz.Game.Bot;
import sk.Aries512.BotWarz.Game.BotCommand;
import java.util.Map;

public interface RemoteServerProtocol {

	public Message decode(String string);

	public String createLoginMessage(String nick, String hash);
	public String createMessageBotUpdate(Map<Bot, BotCommand> commands, String cmdId);

}
