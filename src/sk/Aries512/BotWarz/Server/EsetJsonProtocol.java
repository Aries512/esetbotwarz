package sk.Aries512.BotWarz.Server;

import com.changeme.json.Minify;
import org.json.JSONArray;
import org.json.JSONObject;
import sk.Aries512.BotWarz.Game.Bot;
import sk.Aries512.BotWarz.Game.BotCommand;
import sk.Aries512.BotWarz.Game.GameConstants;
import sk.Aries512.BotWarz.Game.Position;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EsetJsonProtocol implements RemoteServerProtocol {


	private String nickname;
	// we must remember game constants to set correct speed levels to bots
	private GameConstants gameConstants = new GameConstants();


	@Override
	public Message decode(String source) {
		JSONObject obj = new JSONObject(source);
		if (obj.keySet().contains("status")) {
			return parseMeta(obj);
		} else if (obj.keySet().contains("game")) {
			return parseGameInit(obj.getJSONObject("game"));
		} else if (obj.keySet().contains("play")) {
			return parseGameUpdate(obj.getJSONObject("play"));
		} else if (obj.keySet().contains("result")) {
			return parseResult(obj.getJSONObject("result"));
		}
		return new Message();
	}

	private Message parseMeta(JSONObject obj) {
		Message msg = new Message(Message.MessageType.GameUpdate);
		switch (obj.getString("status")) {
			case "socket_connected":
				msg.type = Message.MessageType.LoginPrompt;
				msg.additionalInfo = obj.getString("msg");
				msg.randomStr = obj.getString("random");
				break;
			case "login_ok":
				msg.type = Message.MessageType.LoginAnswer;
				msg.additionalInfo = obj.getString("msg");
				break;
			case "command_error_json":
				msg.type = Message.MessageType.TerminationError;
				msg.additionalInfo = obj.getString("msg");
				break;
			case "command_time_error":
				msg.type = Message.MessageType.TimeError;
				msg.additionalInfo = obj.getString("msg");
				break;
		}
		return msg;
	}

	private Message parseGameInit(JSONObject obj) {
		Message msg = new Message(Message.MessageType.GameStart);
		msg.time = obj.getLong("time");
		msg.gameConstants.botRadius = obj.getInt("botRadius");
		JSONObject worldObj = obj.getJSONObject("world");
		msg.gameConstants.worldWidth = worldObj.getInt("width");
		msg.gameConstants.worldHeight = worldObj.getInt("height");
		JSONArray speedLevelArr = obj.getJSONArray("speedLevels");
		msg.gameConstants.speedLevels = new GameConstants.SpeedLevel[speedLevelArr.length() + 1];
		msg.gameConstants.speedLevels[0] = new GameConstants.SpeedLevel(0, 0);
		for (int i = 0; i < speedLevelArr.length(); i++) {
			JSONObject speedLevelObj = speedLevelArr.getJSONObject(i);
			msg.gameConstants.speedLevels[i + 1] = new GameConstants.SpeedLevel(speedLevelObj.getInt("speed"), speedLevelObj.getInt("maxAngle"));
		}
		this.gameConstants = msg.gameConstants;
		JSONArray playersArr = obj.getJSONArray("players");
		msg.playerBots = parseBotList(playersArr, true);
		msg.enemyBots = parseBotList(playersArr, false);
		return msg;
	}

	private Message parseGameUpdate(JSONObject obj) {
		Message msg = new Message(Message.MessageType.GameUpdate);
		msg.time = obj.getLong("time");
		if (obj.has("lastCmdId")) {
			msg.lastCmdId = Integer.toString(obj.getInt("lastCmdId"));
		}
		JSONArray arr = obj.getJSONArray("players");
		msg.playerBots = parseBotList(arr, true);
		msg.enemyBots = parseBotList(arr, false);
		return msg;
	}

	private List<Bot> parseBotList(JSONArray arr, boolean player) {
		List<Bot> bots = new ArrayList<Bot>();
		for (int i = 0; i < arr.length(); i++) {
			JSONObject obj = arr.getJSONObject(i);
			String nickname = obj.getString("nickname");
			if (player == nickname.equals(this.nickname)) {
				JSONArray botsArr = obj.getJSONArray("bots");
				for (int j = 0; j < botsArr.length(); j++) {
					bots.add(parseBot(botsArr.getJSONObject(j)));
				}
			}
		}
		return bots;
	}

	private Bot parseBot(JSONObject obj) {
		/*
		int speedLevel = 1;
		if (obj.has("speed")) {
			int speed = obj.getInt("speed");
			for (int i = 0; i < gameConstants.speedLevels.length; i++) {
				if (gameConstants.speedLevels[i].speed == speed) {
					speedLevel = i;
					break;
				}
			}
		}
		*/
		int speed = obj.has("speed") ? obj.getInt("speed") : gameConstants.speedLevels[1].speed;
		return new Bot(obj.getInt("id"), new Position((float)obj.getDouble("x"), (float)obj.getDouble("y")), speed, obj.getInt("angle"));
	}

	private Message parseResult(JSONObject obj) {
		Message msg = new Message(Message.MessageType.GameResult);
		msg.time = obj.getLong("time");
		msg.additionalInfo = obj.getString("status");
		String winnerName = obj.getJSONObject("winner").getString("nickname");
		boolean playerWon = winnerName.equals(this.nickname);
		msg.winnerNickname = winnerName;
		JSONArray botsArr = obj.getJSONObject("winner").getJSONArray("bots");
		if (playerWon) {
			msg.playerBots = new ArrayList<Bot>();
		} else {
			msg.enemyBots = new ArrayList<Bot>();
		}
		for (int j = 0; j < botsArr.length(); j++) {
			if (playerWon) {
				msg.playerBots.add(parseBot(botsArr.getJSONObject(j)));
			} else {
				msg.enemyBots.add(parseBot(botsArr.getJSONObject(j)));
			}
		}
		return msg;
	}



	@Override
	public String createLoginMessage(String nickname, String hash) {
		this.nickname = nickname;
		JSONObject obj = new JSONObject();
		obj.put("nickname", nickname);
		obj.put("hash", hash);
		JSONObject par = new JSONObject();
		par.put("login", obj);
		Minify minify = new Minify();
		return minify.minify(par.toString()) + '\n';
	}

	private static Map<BotCommand.BotCommandType, String> commandNames = null;
	static {
		commandNames = new HashMap<BotCommand.BotCommandType, String>();
		commandNames.put(BotCommand.BotCommandType.Accelerate, "accelerate");
		commandNames.put(BotCommand.BotCommandType.Decelerate, "brake");
		commandNames.put(BotCommand.BotCommandType.Turn, "steer");
	}

	public String createMessageBotUpdate(Map<Bot, BotCommand> commands, String cmdId) {
		JSONObject obj = new JSONObject();
		obj.put("cmdId", cmdId);
		JSONArray arr = new JSONArray();
		int pos = 0;
		for (Bot bot: commands.keySet()) {
			BotCommand command = commands.get(bot);
			JSONObject botObj = new JSONObject();
			botObj.put("id", bot.id);
			botObj.put("cmd", commandNames.get(command.type));
			if (command.type == BotCommand.BotCommandType.Turn) {
				botObj.put("angle", command.amount);
			}
			arr.put(pos++, botObj);
		}
		obj.put("bots", arr);
		Minify minify = new Minify();
		return minify.minify(obj.toString()) + '\n';
	}

}
